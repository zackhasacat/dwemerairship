This mod adds in a dwemer airship, made by DimNussens, that can move around the world.

To get started, open the console, and while outside, enter in "luabuildairship"(You can ommit the lua if you are in lua context, with luag or luap), and hit enter.

It will construct the airship above you, and teleport you to it.

Once on the airship, you can take control of it via the U key. When in this mode, you will see a UI with instructions on keybindings.

While in this mode, you can move it however you'd like. 

The airship can also automatically travel. By default, the N key will allow you to loop through destinations, then you can lock in one with the G key.

When you lock in a destination, the airship will rise to crusing altitude, then blast over to its destination.

To create a new destination, move it exactly where you'd like it to be, and type in the console "shippos" or "luashippos" followed by the name of the location, so for example "shippos balmora".



Note, that to reduce loading, you may want to disable the navigator in settings.cfg, or build a cache but I've not had good luck with this.


While on the airship, you can place objects, and they will become part of the airship, moving when it moves.

This is an early version of the airship, just for testing. It should work fine but may introduce some oddities.