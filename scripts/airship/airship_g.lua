local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local interfaces = require("openmw.interfaces")


local locDat = storage.globalSection("airshipLocations")

local acti = require("openmw.interfaces").Activation
local player = nil
local airshipOb = nil
local markerOb = nil
local myDest = nil
local travelHeight = 0
local airshipId = nil
local airshipPx, airshipPy = 0, 0
local fshipObjects = {}
local shipObjectIDs = {}
local destinationPosition = nil
local travelStage = 0
local travelStages = {
    AscendAndAim = 1,
    travelForward = 2,
    rotateToPoint = 3,
    Descent = 4,
}
local dataImport ={{infoName = 'sadrith',px = 140608.36, py = 51562.39, pz = 3528.28,pr =  -0.69},
	{infoName = 'vos',px = 92003.08, py = 73712.83, pz = 3903.22,pr =  -0.80},
    {infoName = 'vivec',px = 32694.28, py = -77244.68, pz = 3425.55,pr =  1.54},
    {infoName = 'balmora',px = -21770.34, py = -18839.44, pz = 1490.65,pr =  -1.55},
    {infoName = 'molag mar',px = 112118.20, py = -58978.28, pz = 2281.66,pr =  1.47},
    {infoName = 'helnim',px = 209754.22, py = 9491.82, pz = 1423.14,pr =  -2.62},
    {infoName = 'wolv',px = 147964.58, py = 28426.36, pz = 3072.09,pr =  0.09},
    
}
local function getCellsAroundOb(centerX, centerY)
    local ret = {}
    local centerCell = world.getExteriorCell(centerX, centerY)
    table.insert(ret, centerCell)

    -- Iterate over the surrounding cells
    for dx = -1, 1 do
        for dy = -1, 1 do
            local cellX = centerX + dx
            local cellY = centerY + dy
            local cell = world.getExteriorCell(cellX, cellY)
            table.insert(ret, cell)
        end
    end

    return ret
end
local function findAirship()

    local addedIds = {}
    for index, cell in ipairs(getCellsAroundOb(airshipPx, airshipPy)) do
        for index, obj in ipairs(cell:getAll()) do
            if obj.recordId == "zh_ship" then
                airshipOb = obj
                --  airshipPx, airshipPy= obj.cell.gridX, obj.cell.gridY
            elseif obj.recordId == "zhac_dna_exitmarker" then
                markerOb = obj
                obj:setScale(0)
            end
            for index, fid in ipairs(shipObjectIDs) do
                if obj.id == fid and addedIds[fid] == nil then
                    table.insert(fshipObjects, obj)
                    addedIds[fid] = true
                end
            end
        end
    end
    if not airshipOb and #shipObjectIDs > 0 then
        return
    end
    world.players[1]:sendEvent("setAirshipOb",airshipOb)
    print(#fshipObjects)
end
local function setPlayerPosition(pos,rotZ)

  local scr = world.mwscript.getGlobalScript("zhac_airship_setppos", world.players[1])
  scr.variables.px = pos.x
  scr.variables.py = pos.y
  scr.variables.pz = pos.z
  scr.variables.pr = math.deg(rotZ)
  scr.variables.domove = 1
  
end
local function teleportObject(rot, pos, ob)
    if ob == world.players[1] then
        if setPlayerPosition ~= nil then
            local rz = 0
            if rot then
                rz = rot:getAnglesZYX()
            else
                rz = world.players[1].rotation:getAnglesZYX()
            end
            setPlayerPosition(pos, rz)
            return

        end
    end
    if not rot then
        ob:teleport(ob.cell, pos)
        return
    end
    ob:teleport(ob.cell, pos, rot)
    --shipObjects[i]:setRotation(rotation)
    --shipObjects[i]:setPosition(position)
end
local function playerCheck()
    local playerFound = false
    for index, value in ipairs(fshipObjects) do
        if value == world.players[1] then
            playerFound = true
        end
    end
    if not playerFound then
        table.insert(fshipObjects, world.players[1])
    end
end
local function distanceBetweenPos(vector1, vector2)
    local dx = vector2.x - vector1.x
    local dy = vector2.y - vector1.y
    return math.sqrt(dx * dx + dy * dy)
end


local function createRotation(x, y, z)
    if (core.API_REVISION < 40) then
        return util.vector3(x, y, z)
    else
        local rotate = util.transform.rotateZ((z))
        return rotate
    end
end
local function distToDest(pos)
    if pos == nil then
        pos = airshipOb.position
    end
    if myDest == nil then
        return -1
    else
        local dist = distanceBetweenPos(myDest.shipLoc, airshipOb.position)
        return dist
    end
end
local function setSiltState(state)
    for index, cell in ipairs(world.cells) do
        if (cell.isExterior and cell.name ~= nil) then
            for index, act in ipairs(cell:getAll(types.Activator)) do
                if act.recordId == "a_siltstrider" then
                    act.enabled = state
                    for index, value in ipairs(cell:getAll(types.NPC)) do
                        if value.type.record(value).class == "caravaner" then
                            value.enabled = state
                        end
                    end
                end
            end
        end
    end
end
local doRot = false
local angle = 0
local function normalizeAngle(angle)
    angle = angle % 360 -- Normalize angle between 0 and 359

    if angle > 180 then
        angle = angle - 360 -- Adjust angles larger than 180
    elseif angle < -180 then
        angle = angle + 360 -- Adjust angles smaller than -180
    end
    -- print(angle)
    return angle
end
local function rotateToFace(object)
    angle = math.atan2(object.position.x - airshipOb.position.x, object.position.y - airshipOb.position.y)
    rotateObjects(math.deg(angle))
end

local function rotateObjects(degrees)
    local shipObjects = fshipObjects
    for index, value in ipairs(fshipObjects) do
        -- table.insert(shipObjects, value)
    end
    local angle = math.rad((degrees))
    local recordId = airshipOb.recordId
    -- Find the object with the specified recordId and calculate its position
    local center = airshipOb.position


    -- Calculate the total offset of all objects from the center
    local totalOffset = util.vector3(0, 0, 0)
    for i = 1, #fshipObjects do
        totalOffset = totalOffset + (shipObjects[i].position - center)
    end

    -- Calculate the average position of all objects
    local averagePosition = center -- + (totalOffset / #shipObjects)

    -- Rotate each object around the average position
    for i = 1, #fshipObjects do
        -- Calculate the relative position of the object to the average position
        local relativePosition = shipObjects[i].position - averagePosition
        local x = relativePosition.x * math.cos(angle) - relativePosition.y * math.sin(angle)
        local y = relativePosition.x * math.sin(angle) + relativePosition.y * math.cos(angle)
        local position = util.vector3(x + averagePosition.x, y + averagePosition.y, shipObjects[i].position.z)

        -- Calculate the new rotation
        local rz, ry, rx = fshipObjects[i].rotation:getAnglesZYX()
        local rotation = createRotation(rx, ry, rz - angle)


        if fshipObjects[i]:isValid() then
            -- Move the object
            teleportObject(rotation, position, shipObjects[i])
        end
    end
end
local function moveAirship(ob, position, rotationZ, doEvent, newCell)
    local rot = createRotation(0, 0, rotationZ)
    --airshipOb = ob

    airshipPx, airshipPy = airshipOb.cell.gridX, airshipOb.cell.gridY
    if (not airshipOb.cell.isExterior) then
        airshipPx, airshipPy = player.cell.gridX, player.cell.gridY
    end
    -- airshipOb:teleport("", position, rot)
    --player:teleport("", util.vector3(position.x, position.y, position.z - 340), rot)
    --player:teleport(player.cell.name, (player.position - airshipOb.position) + position)
    if newCell then
        for index, value in ipairs(fshipObjects) do
            --  value:setPosition(value.position - airshipOb.position + position)
            value:teleport("", (value.position - airshipOb.position) + position)
            if value == markerOb and (newCell or doEvent) then
                player:teleport("", (value.position - airshipOb.position) + position)
            end
        end
        return
    end
    for index, value in ipairs(fshipObjects) do
        teleportObject(nil, value.position - airshipOb.position + position, value)
        --   value:teleport("", (value.position - airshipOb.position) + position)
    end
    teleportObject(nil, world.players[1].position - airshipOb.position + position, world.players[1])
    doRot = true
    if doEvent then
        player:sendEvent("POVAirship")
    end
end

local function getPositionBehind(pos, rot, distance, direction)
    local currentRotation = -rot
    local angleOffset = 0

    if direction == "north" then
        angleOffset = math.rad(90)
    elseif direction == "south" then
        angleOffset = math.rad(-90)
    elseif direction == "east" then
        angleOffset = 0
    elseif direction == "west" then
        angleOffset = math.rad(180)
    else
        error("Invalid direction. Please specify 'north', 'south', 'east', or 'west'.")
    end

    currentRotation = currentRotation - angleOffset
    local obj_x_offset = distance * math.cos(currentRotation)
    local obj_y_offset = distance * math.sin(currentRotation)
    local obj_x_position = pos.x + obj_x_offset
    local obj_y_position = pos.y + obj_y_offset
    return util.vector3(obj_x_position, obj_y_position, pos.z)
end
local function rotateAndMoveAirship(degrees, newPosition, rotationZ, doEvent, newCell)
    --local rot = createRotation(0, 0, rotationZ)
    -- airshipOb = ob
    playerCheck()
    local newPos = {}

    airshipPx, airshipPy = airshipOb.cell.gridX, airshipOb.cell.gridY
    if (not airshipOb.cell.isExterior) then
        airshipPx, airshipPy = player.cell.gridX, player.cell.gridY
    end
    -- airshipOb:teleport("", position, rot)
    --player:teleport("", util.vector3(position.x, position.y, position.z - 340), rot)
    --player:teleport(player.cell.name, (player.position - airshipOb.position) + position)
    if newCell then
        for index, value in ipairs(fshipObjects) do
            --  value:setPosition(value.position - airshipOb.position + position)
            value:teleport("", (value.position - airshipOb.position) + newPosition)
        end
        return
    end
    for index, value in ipairs(fshipObjects) do
        newPos[value.id] = (value.position - airshipOb.position + newPosition)
        --   value:teleport("", (value.position - airshipOb.position) + position)
    end
    if degrees == 0 then
        for index, value in ipairs(fshipObjects) do
            teleportObject(nil, newPos[value.id], value)
        end
        teleportObject(nil, world.players[1].position - airshipOb.position + newPosition, world.players[1])
    else
        local angle = math.rad((degrees))
        local recordId = airshipOb.recordId
        -- Find the object with the specified recordId and calculate its position
        local center = newPos[airshipOb.id]


        -- Calculate the total offset of all objects from the center
        local totalOffset = util.vector3(0, 0, 0)
        for i = 1, #fshipObjects do
            totalOffset = totalOffset + (newPos[fshipObjects[i].id] - center)
        end

        -- Calculate the average position of all objects
        local averagePosition = center -- + (totalOffset / #shipObjects)

        -- Rotate each object around the average position
        for i = 1, #fshipObjects do
            -- Calculate the relative position of the object to the average position
            local relativePosition = newPos[fshipObjects[i].id] - averagePosition
            local x = relativePosition.x * math.cos(angle) - relativePosition.y * math.sin(angle)
            local y = relativePosition.x * math.sin(angle) + relativePosition.y * math.cos(angle)
            local position = util.vector3(x + averagePosition.x, y + averagePosition.y, newPos[fshipObjects[i].id].z)

            -- Calculate the new rotation
            local rz, ry, rx = fshipObjects[i].rotation:getAnglesZYX()
            local rotation = createRotation(rx, ry, rz - angle)


            if fshipObjects[i]:isValid() then
                -- Move the object
                teleportObject(rotation, position, fshipObjects[i])
            end
        end
    end
    doRot = true
    if doEvent then
        player:sendEvent("POVAirship")
    end
end
local function moveAirshipEvent(data)
    rotateAndMoveAirship(data.rot, data.position, data.rotationZ, false, nil)
    -- moveAirship(data.ob, data.position, data.rotationZ)
end
local tdt = 0
local desiredRotation = 999
local function ExitBuildMode(data)
    table.insert(fshipObjects, data.placedItem)
end
local function addObjectToAirship(obj)
    table.insert(shipObjectIDs, obj.id)
    table.insert(fshipObjects, obj)
end
local function removeObjectFromAirship(obj)
    for index, value in ipairs(shipObjectIDs) do
        if value == obj.id then
            table.remove(shipObjectIDs, index)
            break
        end
    end
    for index, value in ipairs(fshipObjects) do
        if value == obj then
            table.remove(fshipObjects, index)
            break
        end
    end
end
local function buildAirship()
    local cplayer = world.players[1]
    --local aOb = world.createObject("dn_airship")
    if #world.getCellByName("Dwemer Airship Exterior"):getAll() == 0 then
        error("Airship is already built")
    end
    if not player.cell.isExterior then
        error("Player must be outside")
    end
    for index, obj in ipairs(world.getCellByName("Dwemer Airship Exterior"):getAll()) do
        if obj.recordId == "zh_ship" then
            airshipOb = obj
            airshipPx, airshipPy = obj.cell.gridX, obj.cell.gridY
            world.players[1]:sendEvent("setAirshipOb",obj)
        elseif obj.recordId == "zhac_dna_exitmarker" then
            markerOb = obj
            obj:setScale(0)
        end
        table.insert(fshipObjects, obj)
        table.insert(shipObjectIDs, obj.id)
    end
    local position = player.position
    local cell = cplayer.cell
    airshipId = airshipOb.id
    airshipPx = cell.posX
    airshipPy = cell.posY
    position = util.vector3(position.x, position.y, position.z + 1200)
    moveAirship(airshipOb, position, 0, false, true)
end
local zMove = 10
local fMove = 140
local lastDist = 0
local startDist = 0
local function calculateTravelSpeed(percentage)
    if percentage <= 5 then
        -- Ramp up from 0.1% to normal speed
        return (percentage / 5) * (fMove) + 0.1
    elseif percentage >= 95 then
        -- Ramp down from normal speed to 0.1%
        return ((100 - percentage) / 5) * (fMove) + 0.1
    else
        -- Maintain normal speed between 5% and 95%
        return fMove
    end
end
local function onUpdate(dt)
    if airshipOb == nil and #shipObjectIDs > 0 then
        findAirship()
        return
    end
    if airshipOb == nil then return end

    local newPos = airshipOb.position
    if dt == 0 then return end

    if myDest then
        local dist = (distToDest())
        if travelStage == travelStages.AscendAndAim then
            if airshipOb.position.z + zMove > travelHeight then
                newPos = util.vector3(airshipOb.position.x, airshipOb.position.y, travelHeight)
                if desiredRotation == 999 then
                    travelStage = travelStages.travelForward
                    startDist = dist
                end
            else
                newPos = util.vector3(airshipOb.position.x, airshipOb.position.y, airshipOb.position.z + zMove)
            end
        elseif travelStage == travelStages.travelForward then
            local actRot            = airshipOb.rotation:getAnglesZYX()
            local currentPercentage = ((dist - startDist) / (0 - startDist)) * 100

            local newPosx           = getPositionBehind(newPos, actRot, calculateTravelSpeed(currentPercentage), "north")
            if (dist > lastDist and lastDist > 0) then
                newPos = util.vector3(myDest.shipLoc.x, myDest.shipLoc.y, airshipOb.position.z)
                travelStage = travelStages.rotateToPoint
                desiredRotation = normalizeAngle(math.deg(myDest.shipRot:getAnglesZYX()))
            else
                newPos = newPosx
            end
            --  print(dist)
            print("travel", calculateTravelSpeed(currentPercentage))
            lastDist = dist
        elseif travelStage == travelStages.rotateToPoint then
            if desiredRotation == 999 then
                travelStage = travelStages.Descent
            end
            print("Rotate", desiredRotation)
        elseif travelStage == travelStages.Descent then
            if airshipOb.position.z + zMove < myDest.shipLoc.z then
                newPos = util.vector3(airshipOb.position.x, airshipOb.position.y, myDest.shipLoc.z)
                travelStage = 0
                myDest = nil
                lastDist = 0
                rotateAndMoveAirship(0, newPos)
            else
                newPos = util.vector3(airshipOb.position.x, airshipOb.position.y, airshipOb.position.z - zMove)
            end
        end
    end
    if airshipOb and desiredRotation ~= 999 then
        local rot = math.deg(airshipOb.rotation:getAnglesZYX())
        local correctedRot = rot + 180
        local correctedDesiredRot = desiredRotation + 180
        if correctedRot - correctedDesiredRot < 1 and correctedRot - correctedDesiredRot > -1 then
            rotateAndMoveAirship(rot - desiredRotation, newPos)
            desiredRotation = 999
            return
        elseif correctedDesiredRot - correctedRot < 1 and correctedDesiredRot - correctedRot > -1 then
            rotateAndMoveAirship(desiredRotation - rot, newPos)
            desiredRotation = 999
            return
        end
        if correctedRot > correctedDesiredRot then
            rotateAndMoveAirship(1, newPos)
        elseif correctedRot == correctedDesiredRot then
            desiredRotation = 999
        else
            rotateAndMoveAirship(-1, newPos)
        end
    elseif airshipOb and myDest then
        rotateAndMoveAirship(0, newPos)
    end
end
local function DNAirshipPlayer(playerf)
    player = playerf
end
local function onPlayerAdded(plr)
    player = plr
end
local function setRot(x, y)
    desiredRotation = normalizeAngle(x)
    if not y then
        return
    end
    airshipOb = y
end
local function onSave()
    return { airshipPx = airshipPx, airshipPy = airshipPy, shipObjectIDs = shipObjectIDs, }
end
local function onItemActive(item)
    if item.cell.isExterior and item.contentFile == nil then
        addObjectToAirship(item)
    end
end
local function navToCell(cellname)
    local cell = world.getCellByName(cellname)
    if cell == nil or not cell.isExterior then return end
    local target = cell:getAll()[1]
    if target then
        desiredRotation = normalizeAngle(math.deg(math.atan2(target.position.x - airshipOb.position.x,
            target.position.y - airshipOb.position.y)) + 180)
        destinationPosition = util.vector3(target.position.x, target.position.y, airshipOb.position.z)
    end
end
local function onLoad(data)
    if not data then return end
    airshipPx = data.airshipPx
    airshipPy = data.airshipPy
    shipObjectIDs = data.shipObjectIDs
    findAirship()
end
local function activateDoor(door, actor)
    if door.cell.name == "Dwemer Airship" and door.recordId == "door_dwe_00_exp" then
        if markerOb == nil then
            error("Door marker is missing")
            return
        end
        actor:teleport(markerOb.cell, markerOb.position, { rotation = markerOb.rotation, onGround = true })
        return false
    end
end
local function onPlayerAdded(plr)

end
local objReplace = {
    ["ex_dwrv_steamstack00"] = "AB_Ex_DwrvSteamstackRod_a",
    ["active_de_p_bed_28"] = "active_de_p_bed_10"
}
local function onObjectActive(object)
    if objReplace[object.recordId] then
        local newThing = world.createObject(objReplace[object.recordId])
        newThing:teleport(object.cell, object.position, object.rotation)
        object:remove()
    end
end

local function saveLocation(infoName,shipPos,shipRZ)
    local data = locDat:getCopy("locationData")
    if not shipPos then
        shipPos = airshipOb.position
    end
    if not shipRZ then
        shipRZ = airshipOb.rotation
    else
        shipRZ = createRotation(0,0,shipRZ)
    end
    if data == nil then
        data = {}
    end
    local cellName = ""
    if airshipOb then
        cellName = airshipOb.cell.name
    end
    local newData = {
        infoName = infoName,
        shipLoc = shipPos,
        shipRot = shipRZ,
        cellName = cellName
    }
    data[infoName:lower()] = newData
    print(infoName)
    locDat:set("locationData", data)
end
local function onInit()
    -- print(world.players, "is player nil")

    local data = locDat:getCopy("locationData")
    if not data or #data == 0 then
    for index, dest in pairs(dataImport) do
        saveLocation(dest.infoName,util.vector3(dest.px,dest.py,dest.pz),dest.pr)
    end
end
end
local function TPToExit()
    world.players[1]:teleport(markerOb.cell, markerOb.position, { rotation = markerOb.rotation, onGround = true })
end
local function navToDest(destN)
    local data = locDat:getCopy("locationData")
    for index, dest in pairs(data) do
        if dest.infoName == destN then
            desiredRotation = normalizeAngle(math.deg(math.atan2(dest.shipLoc.x - airshipOb.position.x,
                dest.shipLoc.y - airshipOb.position.y)) + 180)
            destinationPosition = util.vector3(dest.shipLoc.x, dest.shipLoc.y, airshipOb.position.z)
            travelHeight = dest.shipLoc.z + 4000
            myDest = dest
            travelStage = travelStages.AscendAndAim
        end
    end
end
acti.addHandlerForType(types.Door, activateDoor)
return {
    interfaceName = "DNAirship",
    interface = {
        version = 1,
        setSiltState = setSiltState,
        buildAirship = buildAirship,
        moveAirship = moveAirship,
        setRot = setRot,
        navToCell = navToCell
    },
    engineHandlers = {
        onInit = onInit,
        onUpdate = onUpdate,
        onLoad = onLoad,
        onSave = onSave,
        onPlayerAdded = onPlayerAdded,
        onItemActive = onItemActive,
        onObjectActive = onObjectActive
    },
    eventHandlers = {
        DNAirshipPlayer = DNAirshipPlayer,
        sendPlayer = sendPlayer,
        TPToExit = TPToExit,
        moveAirshipEvent = moveAirshipEvent,
        setSiltState = setSiltState,
        rotateObjects = rotateObjects,
        ExitBuildMode = ExitBuildMode,
        addObjectToAirship = addObjectToAirship,
        saveLocation = saveLocation,
        navToDest = navToDest,
        buildAirship = buildAirship,
    }
}
