local camera = require('openmw.camera')
local core = require('openmw.core')
local input = require('openmw.input')
local util = require('openmw.util')
local self = require('openmw.self')
local types = require('openmw.types')
local ui = require('openmw.ui')
local async = require('openmw.async')
local nearby = require('openmw.nearby')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

local uiUtil = require("scripts.airship.util.ui")
local buttonsAndInfo = nil
local airshipMode = false
local useUI = true
local airshipOb = nil
local useACCam = false
local function onLoad()
    core.sendGlobalEvent("DNAirshipPlayer", self)
end
local function WriteToConsole(text, error)
    if error == true then
        ui.printToConsole(text, ui.CONSOLE_COLOR.Error)
        return
    end
    ui.printToConsole(text, ui.CONSOLE_COLOR.Info)
end
local function setAirshipOb(ob)
airshipOb = ob
end
local function distanceBetweenPos(vector1, vector2)
    --Quick way to find out the distance between two vectors.
    --Very similar to getdistance in mwscript
    local dx = vector2.x - vector1.x
    local dy = vector2.y - vector1.y
    local dz = vector2.z - vector1.z
    return math.sqrt(dx * dx + dy * dy + dz * dz)
end
local function isBelow(sourcePos,ignoreOb)
    if not ignoreOb then
        ignoreOb = self
    end
    if not sourcePos then
        sourcePos = self.position
    end
    local destPos = util.vector3(sourcePos.x,sourcePos.y,sourcePos.z - 200)
    local hit = nearby.castRay(sourcePos,destPos,{ignore = ignoreOb})
    if hit.hitObject and hit.hitObject.recordId == "zh_ship" then
        return true
    else
        return false
    end
end
local function inControlRange(sourcePos)
if not self.cell.isExterior then return false end
if airshipOb == nil then return false end
if not sourcePos then
    sourcePos = self.position
end
local airshipDist = distanceBetweenPos(airshipOb.position,sourcePos)
--return airshipDist < 2000
return isBelow(sourcePos)
end
local keys = {
    RotatePlus = input.KEY.A,
    RotateMinus = input.KEY.D,
    MoveForward = input.KEY.W,
    MoveBackwards = input.KEY.S,
    MoveLeft = input.KEY.Z,
    MoveRight = input.KEY.C,
    MoveUp = input.KEY.Tab,
    MoveDown = input.KEY.LeftShift,
    ChangeSpeed = input.KEY.Space,
    Mount = input.KEY.U,
    changeStaticMode = input.KEY.O,
    TPToExit = input.KEY.M,
    nextDest = input.KEY.N,
    selDest = input.KEY.G,
    toggleUI = input.KEY.V
}
local function drawUI()
    if buttonsAndInfo then
        buttonsAndInfo:destroy()
    end
    if not airshipMode or not useUI then
        return
    end
    local buttonTable = {}
    table.insert(buttonTable,
        string.format("Move Airship Forward and Backwards:  %s, %s", input.getKeyName(keys.MoveForward),
            input.getKeyName(keys.MoveBackwards)))
    table.insert(buttonTable,
        string.format("Rotate Airship Left and Right: %s, %s", input.getKeyName(keys.RotateMinus),
            input.getKeyName(keys.RotatePlus)))
    table.insert(buttonTable,
        string.format("Move Airship Left and Right:  %s, %s", input.getKeyName(keys.MoveLeft),
            input.getKeyName(keys.MoveRight)))
    table.insert(buttonTable,
        string.format("Move Airship Up and Down:  %s, %s", input.getKeyName(keys.MoveUp), input.getKeyName(keys.MoveDown)))
    table.insert(buttonTable, string.format("Change Airship Speed:  %s", input.getKeyName(keys.ChangeSpeed)))
    table.insert(buttonTable, string.format("Switch Camera Mode:  %s", input.getKeyName(keys.changeStaticMode)))
    table.insert(buttonTable, string.format("Toggle Info UI:  %s", input.getKeyName(keys.toggleUI)))
    table.insert(buttonTable, string.format("Select Next destination:  %s", input.getKeyName(keys.nextDest)))
    table.insert(buttonTable, string.format("Start Travel to Selected Destination:  %s", input.getKeyName(keys.selDest)))
    table.insert(buttonTable, string.format("Engage/Disengage Airship Control:  %s", input.getKeyName(keys.Mount)))
    table.insert(buttonTable, "Rotate Airship Left and Right: W/S")
    buttonsAndInfo = uiUtil.renderItemChoice(buttonTable, 0.0, 0.01)
end
local function getPositionBehind(pos, rot, distance, direction)
    local currentRotation = -rot
    local angleOffset = 0

    if direction == "north" then
        angleOffset = math.rad(90)
    elseif direction == "south" then
        angleOffset = math.rad(-90)
    elseif direction == "east" then
        angleOffset = 0
    elseif direction == "west" then
        angleOffset = math.rad(180)
    else
        error("Invalid direction. Please specify 'north', 'south', 'east', or 'west'.")
    end

    currentRotation = currentRotation - angleOffset
    local obj_x_offset = distance * math.cos(currentRotation)
    local obj_y_offset = distance * math.sin(currentRotation)
    local obj_x_position = pos.x + obj_x_offset
    local obj_y_position = pos.y + obj_y_offset
    return util.vector3(obj_x_position, obj_y_position, pos.z)
end
local function createRotation(x, y, z)
    if (core.API_REVISION < 40) then
        return util.vector3(x, y, z)
    else
        local rotate = util.transform.rotateZ(math.rad(z))
        return rotate
    end
end

local function getAirship()
    if (airshipOb ~= nil) then
        return
    end
    for index, value in ipairs(nearby.activators) do
        if value.recordId == "zh_ship" then
            airshipOb = value
            return
        end
    end
end
local function POVAirship()
    getAirship()

    camera.setMode(camera.MODE.Static)
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, false)
    core.sendGlobalEvent("setSiltState", false)
    airshipMode = true
end
local rOffset = 0
local airshipSpeed = {
    slow = { forward = 10, rot = 0.1, vert = 6,rotspeed = 1, },
    medium = { forward = 50, rot = 0.1, vert = 12 ,rotspeed = 3},
    fast = { forward = 100, rot = 0.1, vert = 20,rotspeed = 6 }
}
local speed = airshipSpeed.slow
local wasSpeedChanged
local function changeSpeed()
    if (speed == airshipSpeed.fast) then
        speed = airshipSpeed.slow
        ui.showMessage("Speed: Slow")
    elseif speed == airshipSpeed.slow then
        speed = airshipSpeed.medium
        ui.showMessage("Speed: Medium")
    elseif speed == airshipSpeed.medium then
        speed = airshipSpeed.fast
        ui.showMessage("Speed: Fast")
    end
end
local ASKP = false

local locDat = storage.globalSection("airshipLocations")
local selDest = nil
local function saveData()

    local data = locDat:get("locationData")
    local next = false
    for index, value in pairs(data) do
        local rz = value.shipRot:getAnglesZYX()
        local line = string.format("{infoName = '%s',px = %.2f, py = %.2f, pz = %.2f,pr =  %.2f},", value.infoName,value.shipLoc.x,value.shipLoc.y,value.shipLoc.z,rz)

        print (line)
    end
end
local function onKeyPress(k)
    if k.code == keys.Mount and inControlRange()then
        if airshipMode then
            camera.setMode(camera.MODE.FirstPerson)
            input.setControlSwitch(input.CONTROL_SWITCH.Controls, true)
            airshipMode = false
            ASKP = true
            drawUI()
        else
            core.sendGlobalEvent("addObjectToAirship", self)
            POVAirship()
            drawUI()
        end

    end

    if k.code == keys.toggleUI and airshipMode then
        useUI = not useUI
        drawUI()
    elseif k.code == keys.changeStaticMode and airshipMode then
        if airshipMode then
            if camera.getMode() == camera.MODE.Static then
                camera.setMode(camera.MODE.FirstPerson)
            else
                camera.setMode(camera.MODE.Static)
            end
        end
    elseif k.code == keys.selDest and selDest ~= nil then
        core.sendGlobalEvent("navToDest", selDest)
        ui.showMessage(selDest)
    elseif k.code == keys.nextDest then
        local data = locDat:get("locationData")
        local next = false
        for index, value in pairs(data) do
            if selDest == nil or next == true then
                selDest = value.infoName
                next = false
                break
            elseif selDest == value.infoName then
                next = true
                print("Found it")
            end
            print(index)
        end
        if next then --if need to start over
            for index, value in pairs(data) do
                selDest = value.infoName
                next = false
                break
            end
        end
        ui.showMessage(selDest)
    elseif k.code == keys.TPToExit and airshipMode then
        core.sendGlobalEvent("TPToExit")
    end
end
local rotationAmount = 0.2
local function onFrame(dt)
    if (dt == 0) then
        return
    end
    local multiplier = 100
    local realSpeed = {
        forward = (speed.forward * dt) * multiplier,
        rot = speed.rot * dt,
        vert = speed.vert * dt * multiplier,
        rotspeed = speed.rotspeed * dt * multiplier
    }
    -- print(realSpeed.forward, speed.forward, dt)
    if airshipOb == nil then
        return
    end
    local useOffset   = rOffset * 0.01
    local newPos      = airshipOb.position
    local actRot      = airshipOb.rotation:getAnglesZYX()
    local angleChange = 0
    -- newPos = getPositionBehind(newPos, actRot, speed, "north")
    if airshipMode then
        rOffset = rOffset + -input.getMouseMoveX()

        if input.isKeyPressed(keys.RotateMinus) then
            actRot = actRot - realSpeed.rot
            -- core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = airshipOb.position, rotationZ = actRot })
            -- core.sendGlobalEvent("rotateObjects", -1)
            angleChange = -rotationAmount
        elseif input.isKeyPressed(keys.RotatePlus) then
            actRot = actRot + realSpeed.rot
            --core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = airshipOb.position, rotationZ = actRot })
            --  core.sendGlobalEvent("rotateObjects", 1)
            angleChange = rotationAmount
        end
        if input.isKeyPressed(keys.MoveForward) then
            newPos = getPositionBehind(newPos, actRot, realSpeed.forward, "north")
            -- core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = newPos, rotationZ = actRot })
        elseif input.isKeyPressed(keys.MoveBackwards) then
            newPos = getPositionBehind(newPos, actRot, realSpeed.forward, "south")
            --  core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = newPos, rotationZ = actRot })
        end
        if input.isKeyPressed(keys.MoveLeft) then
            newPos = getPositionBehind(newPos, actRot, realSpeed.forward, "east")
            --  core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = newPos, rotationZ = actRot })
        elseif input.isKeyPressed(keys.MoveRight) then
            newPos = getPositionBehind(newPos, actRot, realSpeed.forward, "west")
            --  core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = newPos, rotationZ = actRot })
        end
        if input.isKeyPressed(keys.MoveUp) then
            newPos = util.vector3(newPos.x, newPos.y, newPos.z + realSpeed.vert)
            --  core.sendGlobalEvent("moveAirshipEvent", { ob = airshipOb, position = newPos, rotationZ = actRot })
        elseif input.isKeyPressed(keys.MoveDown) then
            newPos = util.vector3(newPos.x, newPos.y, newPos.z - realSpeed.vert)
            --
        end
        if input.isKeyPressed(keys.ChangeSpeed) then
            if (not wasSpeedChanged) then
                wasSpeedChanged = true
                changeSpeed()
            end
        elseif input.isKeyPressed(keys.Mount) then

        else
            wasSpeedChanged = false
            ASKP = false
        end

        if camera.getMode() == camera.MODE.Static then
            local pos = util.vector3(newPos.x, newPos.y, newPos.z + 800)
            local camPos = getPositionBehind(pos, actRot + useOffset, 2800, "south")
            camera.setStaticPosition(camPos)
            local rot = actRot + math.rad(180) + useOffset
            camera.setYaw(rot)
            camera.setPitch(0.4)
        end
    else
        if input.isKeyPressed(keys.Mount)  then
            ASKP = true
            return
        else
            ASKP = false
        end
    end

    if newPos ~= airshipOb.position or angleChange ~= 0 then
        core.sendGlobalEvent("moveAirshipEvent",
            { rot = angleChange * realSpeed.rotspeed , ob = airshipOb, position = newPos, rotationZ = actRot })
    end
end

--TODO:
--Build correct airship
--Save objects
--Smooth movement, can rotate and move at once
--Docking locations, automatic

--Reenable SS that we aren't replacing
--Wait to do the ones that are loaded until the object is not in sight or we go into an interior

local function onConsoleCommand(mode, command, selectedObject)

    local words = {}
 

    for word in command:gmatch("%S+") do 
        table.insert(words, word) 
    end
    local restOf = string.sub(command, string.len(words[1]) + 2) 
    if words[1] == "shippos" or words[1] == "luashippos" then
        core.sendGlobalEvent("saveLocation", restOf)
        WriteToConsole("Setting val:" ..restOf)
    elseif words[1] == "buildship" or words[1] == "buildairship"or words[1] == "luabuildship" or words[1] == "luabuildairship"then
        core.sendGlobalEvent("buildAirship")
    elseif words[1] == "savedata" then
        saveData()
    end
end
local function onInit()
    core.sendGlobalEvent("DNAirshipPlayer", self)

end
return {
    interfaceName = "DNAirship",
    interface = {
        version = 1,
        onInit = onInit,
        POVAirship = POVAirship,
    },
    engineHandlers = {
        onLoad = onLoad,
        onInit = onInit,
        onFrame = onFrame,
        onKeyPress = onKeyPress,
        onConsoleCommand = onConsoleCommand,
    },
    eventHandlers = {
        POVAirship = POVAirship,
        setAirshipOb  = setAirshipOb
    }
}
